import {NavLink} from "react-router-dom";
import styles from "./Navbar.module.css";

export function Navbar() {
    return (
        <nav className={styles.navbar}>
            <ul>
                <li>
                    <NavLink exact to="/" activeClassName={styles.active}>Inicio</NavLink>
                </li>
                <li>
                    <NavLink to="/WhoWeAre" activeClassName={styles.active}>Acerca del estudiante</NavLink>
                </li>

            </ul>
        </nav>
    );
}
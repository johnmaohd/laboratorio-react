import styles from "./WhoWeAre.module.css"
import mao from "../images/mao.jpg"
import {Navbar} from "./Navbar";
export function WhoWeAre () {
    return (
        <div className={styles.contenedor}>

           
<Navbar></Navbar>
            <div className={styles.contacto}>
                <img
                    className={styles.imagen}
                    src={mao}
                    width={500}
                    alt={"woman-see-movie"}/>

                <p className={styles.parrafo}>
                    <strong>Proyecto realizado para el curso:</strong> Aplicaciones Web Multiplataforma
                </p>
                <p className={styles.parrafo}>
                    <strong>Por:</strong> John Mauricio Herrera Díaz
                </p>
                <p className={styles.parrafo}>
                    <strong>Profesora:</strong> Elisabeth Tapia Colón
                </p>
                <b className={styles.parrafo}>
                    Máster Universitario en Diseño y Desarrollo de Interfaz de Usuario Web
                </b>
                <b>UNIR</b>
                <div className={styles.linkedin}>
                    <p>Contacto</p><a className={styles.enlace} href="https://www.linkedin.com/in/john-mauricio-h-502537256/"
                       target="_blank" rel="noopener noreferrer">: Linkedin</a>
                </div>

            </div>


        </div>
    )
}

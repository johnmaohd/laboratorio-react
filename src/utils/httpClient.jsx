const API = "https://api.themoviedb.org/3";

export function get(path) {
    return fetch(API + path, {
        headers: {
            Authorization: "Bearer eyJhbGciOiJIUzI1NiJ9.eyJhdWQiOiI3NGMwNDg1MWU4ZDNmNzdhMDg0MzZmZjJlODUwYzk3ZiIsInN1YiI6IjY1MzU5YjU3YzhhNWFjMDBjNTBhNTg2NiIsInNjb3BlcyI6WyJhcGlfcmVhZCJdLCJ2ZXJzaW9uIjoxfQ.KsDPKtcEVK1Rtk4jILQ-HFaDRg3b9Zc-EsaH-HpdSis",
            "Content-Type": "application/json;charset=utf-8",
        },
    }).then((result) => result.json());
}

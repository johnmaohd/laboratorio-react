import { useSearchParams } from "react-router-dom";
import { MoviesGrid } from "../components/MoviesGrid";
import { Search } from "../components/Search";
import { useDebounce } from "../hooks/useDebounce";
import { Navbar } from "../components/Navbar";

export function LandingPage() {
    const [query] = useSearchParams();
    const search = query.get("search");

    const debouncedSearch = useDebounce(search, 300);
    return (
        <main>
            <div style={{ fontSize: 10, textAlign: 'right', paddingRight: 80 }}>



            </div>

            <div>
                <Navbar/>
            <Search />
            <MoviesGrid key={debouncedSearch} search={debouncedSearch} />
        </div>
        </main>
    );
}